# GitLab Pipelines

My sandbox for testing the pipelines on GitLab


## Usage

Check the [.gitlab-ci.yml](https://gitlab.com/rozpo/pipelines/-/blob/main/.gitlab-ci.yml)

## License

[MIT](https://gitlab.com/rozpo/pipelines/-/blob/main/LICENSE)

## Plant UML
```plantuml
@startuml diagram
package "Some Group" {
  HTTP - [First Component]
  [Another Component]
}

node "Other Groups" {
  FTP - [Second Component]
  [First Component] --> FTP
}

cloud {
  [Example 1]
}


database "MySql" {
  folder "This is my folder" {
    [Folder 3]
  }
  frame "Foo" {
    [Frame 4]
  }
}


[Another Component] --> [Example 1]
[Example 1] --> [Folder 3]
[Folder 3] --> [Frame 4]
@enduml
```